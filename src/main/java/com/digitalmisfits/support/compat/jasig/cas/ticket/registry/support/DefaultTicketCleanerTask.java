package com.digitalmisfits.support.compat.jasig.cas.ticket.registry.support;

import org.jasig.cas.ticket.registry.RegistryCleaner;
import org.jasig.cas.ticket.registry.support.DefaultTicketRegistryCleaner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

public class DefaultTicketCleanerTask {

    @Autowired
    private RegistryCleaner registryCleaner;

    @Scheduled(initialDelay = 20000L, fixedRate=5000000L)
    public void clean() {
        registryCleaner.clean();
    }
}
