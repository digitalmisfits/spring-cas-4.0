package com.digitalmisfits.cas.domain.enumeration;

public enum Authority {

    ROLE_ADMINISTRATOR,
    ROLE_USER
}
