package com.digitalmisfits.cas.domain.service;

import com.digitalmisfits.cas.domain.entity.SecurityUser;

public interface SecurityService {

    public SecurityUser load(long userId);
}
