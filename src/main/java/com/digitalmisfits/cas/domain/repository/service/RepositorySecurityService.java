package com.digitalmisfits.cas.domain.repository.service;

import com.digitalmisfits.cas.domain.entity.SecurityUser;
import com.digitalmisfits.cas.domain.repository.SecurityRepository;
import com.digitalmisfits.cas.domain.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RepositorySecurityService implements SecurityService {

    @Autowired
    private SecurityRepository securityRepository;

    public SecurityUser load(long userId) {
        return securityRepository.findOne(userId);
    }
}


