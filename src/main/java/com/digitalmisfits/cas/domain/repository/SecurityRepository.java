package com.digitalmisfits.cas.domain.repository;

import com.digitalmisfits.cas.aop.annotation.ResourceNotFound;
import com.digitalmisfits.cas.domain.entity.SecurityUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@ResourceNotFound
@Transactional(readOnly = true)
public interface SecurityRepository extends JpaRepository<SecurityUser, Long>, JpaSpecificationExecutor<SecurityUser> {

}

