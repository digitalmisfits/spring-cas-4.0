package com.digitalmisfits.cas.domain.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Spring Security Schema http://docs.spring.io/spring-security/site/docs/current/reference/htmlsingle/#user-schema
 */
@Entity
public class SecurityUser implements java.io.Serializable {

    @Id
    private Long id;

    @NotNull
    @Column(nullable = false)
    private String username;

    @NotNull
    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private Boolean enabled = true;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<SecurityAuthority> authorities;


    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Set<SecurityAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<SecurityAuthority> authorities) {
        this.authorities = authorities;
    }
}
