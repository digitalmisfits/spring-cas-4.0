package com.digitalmisfits.cas.domain.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Spring Security Schema http://docs.spring.io/spring-security/site/docs/current/reference/htmlsingle/#user-schema
 */
@Entity
public class SecurityAuthority implements java.io.Serializable {

    @Id
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private SecurityUser user;

    @NotNull
    @Column(nullable = false)
    private String authority;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
