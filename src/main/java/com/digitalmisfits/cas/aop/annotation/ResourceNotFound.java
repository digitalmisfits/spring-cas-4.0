package com.digitalmisfits.cas.aop.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@Inherited
@Documented
public @interface ResourceNotFound {

    /**
     * Default value (enabled)
     *
     * @return
     */
    boolean value() default true;

    /**
     * Resource type
     *
     * @return
     */
    Class<?> resource() default Object.class;

    /**
     * Throwable type
     *
     * @return
     */
    Class<?> throwable() default RuntimeException.class;

}
