package com.digitalmisfits.cas.web.controller;

import com.digitalmisfits.cas.domain.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.Locale;

@Controller
@RequestMapping("/")
public class DefaultController {

    @Autowired
    private SecurityService securityService;

    @Autowired
    private MessageSource messages;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Authentication index(Principal principal) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication;
    }
}