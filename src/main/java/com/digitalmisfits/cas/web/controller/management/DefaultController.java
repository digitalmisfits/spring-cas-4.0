package com.digitalmisfits.cas.web.controller.management;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Locale;

@Controller(value = "ManagementDefaultController")
@RequestMapping("/management")
public class DefaultController {

    @Autowired
    private MessageSource messages;

    @RequestMapping(value = "/locale", method = RequestMethod.GET)
    @ResponseBody
    public String index(Locale locale) {
        return String.format("(Management) Current locale: %s - %s", locale.getLanguage(), messages.getMessage("label.language", new Object[]{}, locale));
    }
}