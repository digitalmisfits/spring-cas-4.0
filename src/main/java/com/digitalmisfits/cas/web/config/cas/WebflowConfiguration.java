package com.digitalmisfits.cas.web.config.cas;

import com.digitalmisfits.support.compat.jasig.cas.web.flow.CasDefaultFlowUrlHandler;
import org.jasig.cas.CentralAuthenticationService;
import org.jasig.cas.logout.LogoutManager;
import org.jasig.cas.services.ServicesManager;
import org.jasig.cas.ticket.registry.TicketRegistry;
import org.jasig.cas.util.SpringAwareMessageMessageInterpolator;
import org.jasig.cas.util.UniqueTicketIdGenerator;
import org.jasig.cas.web.FlowExecutionExceptionResolver;
import org.jasig.cas.web.flow.*;
import org.jasig.cas.web.support.ArgumentExtractor;
import org.jasig.cas.web.support.CookieRetrievingCookieGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.binding.convert.ConversionService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.ResourceBundleViewResolver;
import org.springframework.webflow.context.servlet.FlowUrlHandler;
import org.springframework.webflow.definition.registry.FlowDefinitionRegistry;
import org.springframework.webflow.engine.builder.ViewFactoryCreator;
import org.springframework.webflow.execution.FlowExecutionListenerAdapter;
import org.springframework.webflow.executor.FlowExecutor;
import org.springframework.webflow.expression.spel.WebFlowSpringELExpressionParser;
import org.springframework.webflow.mvc.builder.MvcViewFactoryCreator;
import org.springframework.webflow.mvc.servlet.FlowHandlerAdapter;
import org.springframework.webflow.mvc.servlet.FlowHandlerMapping;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ImportResource("classpath:/cas-4.0/webflow/webflow-configuration.xml")
public class WebflowConfiguration {

    @Bean
    public HandlerExceptionResolver errorHandlerResolver() {
        return new FlowExecutionExceptionResolver();
    }

    @Bean
    public ViewFactoryCreator viewFactoryCreator() {

        List<ViewResolver> viewResolverList = new ArrayList<>();
        viewResolverList.add(viewResolver());

        MvcViewFactoryCreator mvcViewFactoryCreator = new MvcViewFactoryCreator();
        mvcViewFactoryCreator.setViewResolvers(viewResolverList);

        return mvcViewFactoryCreator;
    }

    @Bean
    public ViewResolver viewResolver() {
        ResourceBundleViewResolver viewResolver = new ResourceBundleViewResolver();
        viewResolver.setBasenames(
                "view-default",
                "view-protocol"
        );
        return viewResolver;
    }

    /*
    @Bean
    public ResourceBundleThemeSource themeSource() {
        ResourceBundleThemeSource themeSource = new ResourceBundleThemeSource();
        return themeSource;
    }
    */

    /* Login Webflow */

    @Bean
    @Autowired
    public FlowHandlerMapping loginFlowHandlerMapping(@Qualifier("loginFlowRegistry")
            FlowDefinitionRegistry loginFlowRegistry) {
        FlowHandlerMapping flowHandlerMapping = new FlowHandlerMapping();
        flowHandlerMapping.setFlowRegistry(loginFlowRegistry);
        flowHandlerMapping.setOrder(0);
        return flowHandlerMapping;
    }

    @Bean
    @Autowired
    public FlowHandlerAdapter loginHandlerAdapter(@Qualifier("logoutFlowRegistry")
            FlowDefinitionRegistry logoutFlowRegistry, @Qualifier("loginFlowExecutor")
            FlowExecutor loginFlowExecutor) {
        SelectiveFlowHandlerAdapter flowHandlerAdapter = new SelectiveFlowHandlerAdapter();
        flowHandlerAdapter.setSupportedFlowId("cas/login");
        flowHandlerAdapter.setFlowExecutor(loginFlowExecutor);
        flowHandlerAdapter.setFlowUrlHandler(loginFlowUrlHandler());

        return flowHandlerAdapter;
    }

    @Bean
    public FlowUrlHandler loginFlowUrlHandler() {
        return new CasDefaultFlowUrlHandler();
    }

    /* Logout WebFlow */


    @Bean
    @Autowired
    public FlowHandlerMapping logoutFlowHandlerMapping(@Qualifier("logoutFlowRegistry")
            FlowDefinitionRegistry logoutFlowRegistry) {
        FlowHandlerMapping flowHandlerMapping = new FlowHandlerMapping();
        flowHandlerMapping.setFlowRegistry(logoutFlowRegistry);
        flowHandlerMapping.setOrder(1);
        return flowHandlerMapping;
    }

    @Bean
    public FlowHandlerAdapter logoutHandlerAdapter(@Qualifier("logoutFlowExecutor")
            FlowExecutor logoutFlowExecutor) {
        SelectiveFlowHandlerAdapter flowHandlerAdapter = new SelectiveFlowHandlerAdapter();
        flowHandlerAdapter.setSupportedFlowId("cas/logout");
        flowHandlerAdapter.setFlowExecutor(logoutFlowExecutor);
        flowHandlerAdapter.setFlowUrlHandler(logoutFlowUrlHandler());
        return flowHandlerAdapter;
    }

    @Bean
    public FlowUrlHandler logoutFlowUrlHandler() {
        CasDefaultFlowUrlHandler flowUrlHandler = new CasDefaultFlowUrlHandler();
        flowUrlHandler.setFlowExecutionKeyParameter("RelayState");
        return flowUrlHandler;
    }

    @Bean
    public WebFlowSpringELExpressionParser expressionParser() {
        return new WebFlowSpringELExpressionParser(
                new SpelExpressionParser(), logoutConversionService()
        );
    }

    @Bean
    public ConversionService logoutConversionService() {
        return new LogoutConversionService();
    }

    /**
     * https://www.unicon.net/about/blog/moodles-race-with-cas-server
     *
     * @return
     */
    @Bean
    public FlowExecutionListenerAdapter terminateWebSessionListener() {
        TerminateWebSessionListener sessionListener = new TerminateWebSessionListener();
        sessionListener.setTimeToDieInSeconds(0);
        return sessionListener;
    }

    @Bean
    @Autowired
    public LogoutAction logoutAction(ServicesManager servicesManager) {
        LogoutAction action = new LogoutAction();
        action.setServicesManager(servicesManager);
        action.setFollowServiceRedirects(false);
        return action;
    }

    @Bean
    @Autowired
    public FrontChannelLogoutAction frontChannelLogoutAction(LogoutManager logoutManager) {
        return new FrontChannelLogoutAction(logoutManager);
    }

    @Bean
    @Autowired
    public InitialFlowSetupAction initialFlowSetupAction(
            List<ArgumentExtractor> argumentExtractors,
            @Qualifier("warnCookieGenerator") CookieRetrievingCookieGenerator warnCookieGenerator,
            @Qualifier("ticketGrantingTicketCookieGenerator") CookieRetrievingCookieGenerator ticketGrantingTicketCookieGenerator) {

        InitialFlowSetupAction action = new InitialFlowSetupAction();
        action.setArgumentExtractors(argumentExtractors);
        action.setWarnCookieGenerator(warnCookieGenerator);
        action.setTicketGrantingTicketCookieGenerator(ticketGrantingTicketCookieGenerator);

        return action;
    }

    @Bean
    @Autowired
    public AuthenticationViaFormAction authenticationViaFormAction(
            CentralAuthenticationService centralAuthenticationService,
            @Qualifier("warnCookieGenerator") CookieRetrievingCookieGenerator warnCookieGenerator,
            @Qualifier("ticketRegistry") TicketRegistry ticketRegistry) {

        AuthenticationViaFormAction action = new AuthenticationViaFormAction();
        action.setCentralAuthenticationService(centralAuthenticationService);
        action.setWarnCookieGenerator(warnCookieGenerator);
        action.setTicketRegistry(ticketRegistry);

        return action;
    }

    @Bean
    public AuthenticationExceptionHandler authenticationExceptionHandler() {
        return new AuthenticationExceptionHandler();
    }


    @Bean
    @Autowired
    public GenerateServiceTicketAction generateServiceTicketAction(
            CentralAuthenticationService centralAuthenticationService) {

        GenerateServiceTicketAction action = new GenerateServiceTicketAction();
        action.setCentralAuthenticationService(centralAuthenticationService);

        return action;
    }

    @Bean
    @Autowired
    public SendTicketGrantingTicketAction sendTicketGrantingTicketAction(
            CentralAuthenticationService centralAuthenticationService,
            @Qualifier("ticketGrantingTicketCookieGenerator") CookieRetrievingCookieGenerator ticketGrantingTicketCookieGenerator) {

        SendTicketGrantingTicketAction action = new SendTicketGrantingTicketAction();
        action.setCentralAuthenticationService(centralAuthenticationService);
        action.setTicketGrantingTicketCookieGenerator(ticketGrantingTicketCookieGenerator);

        return action;
    }


    @Bean
    @Autowired
    public GatewayServicesManagementCheck gatewayServicesManagementCheck(
            ServicesManager servicesManager) {

        return new GatewayServicesManagementCheck(servicesManager);
    }

    @Bean
    @Autowired
    public ServiceAuthorizationCheck serviceAuthorizationCheck(
            ServicesManager servicesManager) {

        return new ServiceAuthorizationCheck(servicesManager);
    }

    @Bean
    @Autowired
    public GenerateLoginTicketAction generateLoginTicketAction(
            @Qualifier("loginTicketUniqueIdGenerator") UniqueTicketIdGenerator loginTicketUniqueIdGenerator) {

        GenerateLoginTicketAction action = new GenerateLoginTicketAction();
        action.setTicketIdGenerator(loginTicketUniqueIdGenerator);

        return action;
    }


    @Bean
    public SpringAwareMessageMessageInterpolator messageInterpolator() {
        return new SpringAwareMessageMessageInterpolator();
    }

    @Bean
    public LocalValidatorFactoryBean credentialsValidator() {
        LocalValidatorFactoryBean validatorFactoryBean = new LocalValidatorFactoryBean();
        validatorFactoryBean.setMessageInterpolator(messageInterpolator());
        return validatorFactoryBean;
    }

    @Bean
    @Autowired
    public TicketGrantingTicketCheckAction ticketGrantingTicketCheckAction(
            @Qualifier("ticketRegistry") TicketRegistry ticketRegistry) {

        return new TicketGrantingTicketCheckAction(ticketRegistry);
    }

    @Bean
    @Autowired
    public TerminateSessionAction terminateSessionAction(
            CentralAuthenticationService centralAuthenticationService,
            @Qualifier("warnCookieGenerator") CookieRetrievingCookieGenerator warnCookieGenerator,
            @Qualifier("ticketGrantingTicketCookieGenerator") CookieRetrievingCookieGenerator ticketGrantingTicketCookieGenerator) {

        return new TerminateSessionAction(centralAuthenticationService,
                ticketGrantingTicketCookieGenerator, warnCookieGenerator);
    }
}
