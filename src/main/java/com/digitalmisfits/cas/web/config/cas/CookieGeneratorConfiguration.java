package com.digitalmisfits.cas.web.config.cas;

import org.jasig.cas.web.support.CookieRetrievingCookieGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.CookieGenerator;


@Configuration
public class CookieGeneratorConfiguration {

    /**
     * Defines the cookie that stores the TicketGrantingTicket. You most likely should never modify these (especially the "secure" property).
     * You can change the name if you want to make it harder for people to guess.
     */
    @Bean
    public CookieRetrievingCookieGenerator ticketGrantingTicketCookieGenerator() {
        CookieRetrievingCookieGenerator cookieGenerator = new CookieRetrievingCookieGenerator();
        cookieGenerator.setCookieSecure(true);
        cookieGenerator.setCookieMaxAge(-1);
        cookieGenerator.setCookieName("CASTGC");
        cookieGenerator.setCookiePath("/cas/");
        return cookieGenerator;
    }

    /**
     * Spring Configuration file describes the cookie used to store the WARN parameter so that a user is warned whenever the CAS service
     * is used. You would modify this if you wanted to change the cookie path or the name.
     */
    @Bean
    public CookieRetrievingCookieGenerator warnCookieGenerator() {
        CookieRetrievingCookieGenerator cookieGenerator = new CookieRetrievingCookieGenerator();
        cookieGenerator.setCookieSecure(true);
        cookieGenerator.setCookieMaxAge(-1);
        cookieGenerator.setCookieName("CASPRIVACY");
        cookieGenerator.setCookiePath("/cas/");
        return cookieGenerator;
    }
}
