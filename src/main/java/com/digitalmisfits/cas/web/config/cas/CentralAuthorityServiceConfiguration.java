package com.digitalmisfits.cas.web.config.cas;

import com.digitalmisfits.support.compat.jasig.cas.ticket.registry.support.DefaultTicketCleanerTask;
import com.google.common.collect.Lists;
import org.jasig.cas.CentralAuthenticationService;
import org.jasig.cas.CentralAuthenticationServiceImpl;
import org.jasig.cas.authentication.*;
import org.jasig.cas.authentication.handler.support.HttpBasedServiceCredentialsAuthenticationHandler;
import org.jasig.cas.authentication.principal.*;
import org.jasig.cas.logout.LogoutManager;
import org.jasig.cas.logout.LogoutManagerImpl;
import org.jasig.cas.logout.LogoutMessageCreator;
import org.jasig.cas.logout.SamlCompliantLogoutMessageCreator;
import org.jasig.cas.monitor.HealthCheckMonitor;
import org.jasig.cas.monitor.HealthStatus;
import org.jasig.cas.monitor.Monitor;
import org.jasig.cas.services.*;
import org.jasig.cas.ticket.proxy.ProxyHandler;
import org.jasig.cas.ticket.proxy.support.Cas10ProxyHandler;
import org.jasig.cas.ticket.proxy.support.Cas20ProxyHandler;
import org.jasig.cas.util.HttpClient;
import org.jasig.cas.util.SimpleHttpClient;
import org.jasig.services.persondir.IPersonAttributeDao;
import org.jasig.services.persondir.support.StubPersonAttributeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
@ComponentScan
public class CentralAuthorityServiceConfiguration {

    @Autowired
    private TicketRegistryConfiguration ticketRegistryConfiguration;

    @Autowired
    private UniqueIdGeneratorsConfiguration uniqueIdGeneratorsConfig;

    @Autowired
    private TicketExpirationPoliciesConfiguration ticketExpirationPoliciesConfiguration;

    @Bean
    public ServicesManager servicesManager() {
        return new DefaultServicesManagerImpl(serviceRegistryDao());
    }


    /**
     * The authentication manager defines security policy for authentication by specifying at a minimum
     * the authentication handlers that will be used to authenticate credential. While the AuthenticationManager
     * interface supports plugging in another implementation, the default PolicyBasedAuthenticationManager should
     * be sufficient in most cases.
     * <p/>
     * IMPORTANT
     * Every handler requires a unique name.
     * If more than one instance of the same handler class is configured, you must explicitly
     * set its name to something other than its default name (typically the simple class name).
     *
     * @return
     */
    @Bean
    public AuthenticationManager authenticationManager() {

        Map<AuthenticationHandler, PrincipalResolver> resolverMap = new HashMap<>();
        resolverMap.put(primaryAuthenticationHandler(), primaryPrincipalResolver());
        resolverMap.put(proxyAuthenticationHandler(), proxyPrincipalResolver());

        PolicyBasedAuthenticationManager authenticationManager = new PolicyBasedAuthenticationManager(resolverMap);
        authenticationManager.setAuthenticationPolicy(authenticationPolicy());

        return authenticationManager;
    }


    @Bean
    public AuthenticationPolicy authenticationPolicy() {
        return new AnyAuthenticationPolicy();
    }

    @Bean
    public AuthenticationHandler proxyAuthenticationHandler() {
        HttpBasedServiceCredentialsAuthenticationHandler authenticationHandler = new HttpBasedServiceCredentialsAuthenticationHandler();
        authenticationHandler.setHttpClient(httpClient());
        return authenticationHandler;
    }

    @Bean
    public AuthenticationHandler primaryAuthenticationHandler() {
        Map<String, String> users = new HashMap<>();
        users.put("cas-user", "Mellon");
        AcceptUsersAuthenticationHandler authenticationHandler = new AcceptUsersAuthenticationHandler();
        authenticationHandler.setUsers(users);
        return authenticationHandler;
    }

    @Bean
    public PrincipalResolver proxyPrincipalResolver() {
        return new BasicPrincipalResolver();
    }

    @Bean
    public PrincipalResolver primaryPrincipalResolver() {
        PersonDirectoryPrincipalResolver principalResolver = new PersonDirectoryPrincipalResolver();
        principalResolver.setAttributeRepository(attributeRepository());
        return principalResolver;
    }

    @Bean
    public IPersonAttributeDao attributeRepository() {

        // Authorities
        List<Object> authorities = new ArrayList<>();
        authorities.add("ADMINISTRATOR");

        // Backing Map
        Map<String, List<Object>> backingMap = new HashMap<>();
        backingMap.put("authorities", authorities);

        return new StubPersonAttributeDao(backingMap);
    }

    @Bean
    public ServiceRegistryDao serviceRegistryDao() {
        InMemoryServiceRegistryDaoImpl serviceRegistryDao = new InMemoryServiceRegistryDaoImpl();
        serviceRegistryDao.setRegisteredServices(registeredServicesList());
        return serviceRegistryDao;
    }

    @Bean
    public List<RegisteredService> registeredServicesList() {
        List<RegisteredService> registeredServiceList = new ArrayList<>();

        RegexRegisteredService registeredService = new RegexRegisteredService();
        registeredService.setId(0);
        registeredService.setName("HTTP and IMAP");
        registeredService.setDescription("Allows HTTP(S) and IMAP(S) protocols");
        registeredService.setServiceId("^(https?|imaps?)://.*");
        registeredService.setEvaluationOrder(10000001);
        registeredService.setAllowedAttributes(Lists.newArrayList("authorities"));

        registeredServiceList.add(registeredService);
        return registeredServiceList;
    }

    /**
     * <bean id="healthCheckMonitor" class="org.jasig.cas.monitor.HealthCheckMonitor" p:monitors-ref="monitorsList" />
     *
     * @return
     */
    public Monitor<HealthStatus> healthCheckMonitor() {
        HealthCheckMonitor healthCheckMonitor = new HealthCheckMonitor();
        //healthCheckMonitor.setMonitors();
        return healthCheckMonitor;
    }

    @Bean
    public SimpleHttpClient httpClient() {
        SimpleHttpClient httpClient = new SimpleHttpClient();
        httpClient.setReadTimeout(5000);
        httpClient.setConnectionTimeout(5000);
        return httpClient;
    }

    @Bean
    public HttpClient noRedirectHttpClient() {
        SimpleHttpClient httpClient = httpClient();
        httpClient.setReadTimeout(5000);
        httpClient.setConnectionTimeout(5000);
        httpClient.setFollowRedirects(false);
        return httpClient;
    }

    @Bean
    public PersistentIdGenerator persistentIdGenerator() {
        ShibbolethCompatiblePersistentIdGenerator persistentIdGenerator = new ShibbolethCompatiblePersistentIdGenerator();
        persistentIdGenerator.setSalt("cas-4.0");
        return persistentIdGenerator;
    }

    @Bean
    public LogoutManager logoutManager() {
        LogoutManagerImpl logoutManager = new LogoutManagerImpl(servicesManager(), noRedirectHttpClient(), logoutBuilder());
        logoutManager.setDisableSingleSignOut(true);
        return logoutManager;
    }

    @Bean
    public LogoutMessageCreator logoutBuilder() {
        return new SamlCompliantLogoutMessageCreator();
    }

    @Bean
    public ProxyHandler proxy10Handler() {
        return new Cas10ProxyHandler();
    }

    @Bean
    public ProxyHandler proxy20Handler() {
        Cas20ProxyHandler proxyHandler = new Cas20ProxyHandler();
        proxyHandler.setHttpClient(httpClient());
        proxyHandler.setUniqueTicketIdGenerator(uniqueIdGeneratorsConfig.proxy20TicketUniqueIdGenerator());
        return proxyHandler;
    }

    @Bean
    public CentralAuthenticationService centralAuthenticationService() {

        CentralAuthenticationServiceImpl authenticationService = new CentralAuthenticationServiceImpl(
                ticketRegistryConfiguration.ticketRegistry(),
                null,
                authenticationManager(),
                uniqueIdGeneratorsConfig.ticketGrantingTicketUniqueIdGenerator(),
                uniqueIdGeneratorsConfig.uniqueIdGeneratorsMap(),
                ticketExpirationPoliciesConfiguration.grantingTicketExpirationPolicy(),
                ticketExpirationPoliciesConfiguration.serviceTicketExpirationPolicy(),
                servicesManager(),
                logoutManager()
        );

        authenticationService.setPersistentIdGenerator(persistentIdGenerator());

        return authenticationService;
    }
}
