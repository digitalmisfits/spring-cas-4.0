package com.digitalmisfits.cas.web.config;


import com.digitalmisfits.cas.web.config.cas.client.CentralAuthorityServiceClientConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.cas.authentication.CasAuthenticationProvider;
import org.springframework.security.cas.web.CasAuthenticationEntryPoint;
import org.springframework.security.cas.web.CasAuthenticationFilter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@Import(CentralAuthorityServiceClientConfiguration.class)
//@EnableGlobalMethodSecurity(mode = AdviceMode.ASPECTJ, prePostEnabled = true, securedEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {


    @Autowired
    @Qualifier("casEntryPoint")
    private CasAuthenticationEntryPoint casEntryPoint;

    @Autowired
    private CasAuthenticationFilter casAuthenticationFilter;

    @Autowired
    private CasAuthenticationProvider casAuthenticationProvider;


    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws  Exception{
        auth.authenticationProvider(casAuthenticationProvider);
        //auth.inMemoryAuthentication().withUser("test").password("123456").roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {


        http
                .requiresChannel().anyRequest().requiresSecure()
                .and()
                    .antMatcher("/*")
                    .authorizeRequests()
                    .anyRequest()
                    .authenticated();

        http
                .httpBasic()
                    .authenticationEntryPoint(casEntryPoint)
                .and()
                    .addFilter(casAuthenticationFilter);

        http
                .csrf().disable()
                .logout().disable();
    }

    @Bean(name = "myAuthenticationManager")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
