package com.digitalmisfits.cas.web.config;

import com.digitalmisfits.cas.web.config.cas.CentralAuthorityServiceConfiguration;
import com.digitalmisfits.cas.web.config.cas.client.CentralAuthorityServiceClientConfiguration;
import org.jasig.cas.CentralAuthenticationService;
import org.jasig.cas.ticket.proxy.ProxyHandler;
import org.jasig.cas.ticket.registry.TicketRegistry;
import org.jasig.cas.validation.Cas20WithoutProxyingValidationSpecification;
import org.jasig.cas.web.ServiceValidateController;
import org.jasig.cas.web.StatisticsController;
import org.jasig.cas.web.support.ArgumentExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.mvc.AbstractController;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import java.util.*;

@Configuration
@ComponentScan(basePackageClasses = {
        com.digitalmisfits.cas.security.Marker.class,
        com.digitalmisfits.cas.web.controller.management.Marker.class,
        com.digitalmisfits.cas.web.controller.api.Marker.class,
        com.digitalmisfits.cas.web.controller.Marker.class
})
@Import(CentralAuthorityServiceConfiguration.class)
public class WebMvcConfiguration extends WebMvcConfigurationSupport {

    @Bean
    public SessionLocaleResolver localeResolver() {
        SessionLocaleResolver localeResolver = new SessionLocaleResolver();
        localeResolver.setDefaultLocale(new Locale("nl", "NL"));
        return localeResolver;
    }

    @Bean
    public ViewResolver internalViewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/view/jsp/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

    @Bean
    @Autowired
    public SimpleUrlHandlerMapping simpleUrlHandlerMapping(
            StatisticsController statisticsController,
            ServiceValidateController serviceValidateController,
            ServiceValidateController v3ProxyValidateController,
            ServiceValidateController v3ServiceValidateController) {

        SimpleUrlHandlerMapping handlerMapping = new SimpleUrlHandlerMapping();
        handlerMapping.setAlwaysUseFullPath(true);
        handlerMapping.setOrder(2);

        Map<String, AbstractController> mapping = new HashMap<>();
        mapping.put("/cas/statistics", statisticsController);
        mapping.put("/cas/serviceValidate", serviceValidateController);
        mapping.put("/cas/p3/serviceValidate", v3ServiceValidateController);
        mapping.put("/cas/p3/proxyValidate", v3ProxyValidateController);
        handlerMapping.setUrlMap(mapping);

        return handlerMapping;
    }

    @Bean
    @Override
    public RequestMappingHandlerMapping requestMappingHandlerMapping() {
        RequestMappingHandlerMapping handlerMapping = new RequestMappingHandlerMapping();
        handlerMapping.setOrder(3);
        handlerMapping.setInterceptors(getInterceptors());
        handlerMapping.setDetectHandlerMethodsInAncestorContexts(true);
        return handlerMapping;
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> httpMessageConverters) {
        addDefaultHttpMessageConverters(httpMessageConverters);
    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.ignoreAcceptHeader(true);
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("/WEB-INF/static/");
    }

    /* CAS controllers */

    @Bean
    @Autowired
    public StatisticsController statisticsController(
            TicketRegistry ticketRegistry) {
        return new StatisticsController(ticketRegistry);
    }

    @Bean
    @Autowired
    public ServiceValidateController serviceValidateController(
            CentralAuthenticationService centralAuthenticationService,
            @Qualifier("proxy20Handler") ProxyHandler proxy20Handler,
            ArgumentExtractor casArgumentExtractor) {
        ServiceValidateController serviceValidateController = new ServiceValidateController();
        serviceValidateController.setCentralAuthenticationService(centralAuthenticationService);
        serviceValidateController.setProxyHandler(proxy20Handler);
        serviceValidateController.setArgumentExtractor(casArgumentExtractor);
        return serviceValidateController;

    }

    public ServiceValidateController v3AbstractValidateController(
            CentralAuthenticationService centralAuthenticationService,
            @Qualifier("proxy20Handler") ProxyHandler proxy20Handler,
            ArgumentExtractor casArgumentExtractor) {
        ServiceValidateController serviceValidateController = new ServiceValidateController();
        serviceValidateController.setCentralAuthenticationService(centralAuthenticationService);
        serviceValidateController.setProxyHandler(proxy20Handler);
        serviceValidateController.setArgumentExtractor(casArgumentExtractor);
        serviceValidateController.setSuccessView("cas3ServiceSuccessView");
        serviceValidateController.setFailureView("cas3ServiceFailureView");
        return serviceValidateController;
    }

    @Bean
    @Autowired
    public ServiceValidateController v3ProxyValidateController(
            CentralAuthenticationService centralAuthenticationService,
            @Qualifier("proxy20Handler") ProxyHandler proxy20Handler,
            ArgumentExtractor casArgumentExtractor) {

        return v3AbstractValidateController(centralAuthenticationService, proxy20Handler, casArgumentExtractor);
    }

    @Bean
    @Autowired
    public ServiceValidateController v3ServiceValidateController(
            CentralAuthenticationService centralAuthenticationService,
            @Qualifier("proxy20Handler") ProxyHandler proxy20Handler,
            ArgumentExtractor casArgumentExtractor) {

        ServiceValidateController validateController = v3AbstractValidateController(
                centralAuthenticationService,
                proxy20Handler,
                casArgumentExtractor
        );
        validateController.setValidationSpecificationClass(Cas20WithoutProxyingValidationSpecification.class);

        return validateController;
    }
}