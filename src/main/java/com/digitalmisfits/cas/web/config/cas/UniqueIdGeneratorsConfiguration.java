package com.digitalmisfits.cas.web.config.cas;

import org.jasig.cas.authentication.principal.PersistentIdGenerator;
import org.jasig.cas.authentication.principal.ShibbolethCompatiblePersistentIdGenerator;
import org.jasig.cas.util.DefaultUniqueTicketIdGenerator;
import org.jasig.cas.util.UniqueTicketIdGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class UniqueIdGeneratorsConfiguration {

    @Bean
    public UniqueTicketIdGenerator ticketGrantingTicketUniqueIdGenerator() {
        return new DefaultUniqueTicketIdGenerator(50);
    }

    @Bean
    public UniqueTicketIdGenerator serviceTicketUniqueIdGenerator() {
        return new DefaultUniqueTicketIdGenerator(20);
    }

    @Bean
    public UniqueTicketIdGenerator loginTicketUniqueIdGenerator() {
        return new DefaultUniqueTicketIdGenerator(30);
    }

    @Bean
    public UniqueTicketIdGenerator proxy20TicketUniqueIdGenerator() {
        return new DefaultUniqueTicketIdGenerator(20);
    }

    @Bean
    public Map<String, UniqueTicketIdGenerator> uniqueIdGeneratorsMap() {
        Map<String, UniqueTicketIdGenerator> uniqueTicketIdGeneratorMap = new HashMap<>();
        uniqueTicketIdGeneratorMap.put("org.jasig.cas.authentication.principal.SimpleWebApplicationServiceImpl", serviceTicketUniqueIdGenerator());
        return uniqueTicketIdGeneratorMap;
    }
}
