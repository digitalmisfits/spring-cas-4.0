package com.digitalmisfits.cas.web.config.cas;

import org.jasig.cas.web.support.ArgumentExtractor;
import org.jasig.cas.web.support.CasArgumentExtractor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class ArgumentExtractorConfiguration {

    @Bean
    public ArgumentExtractor casArgumentExtractor() {
        return new CasArgumentExtractor();
    }

    @Bean
    public List<ArgumentExtractor> argumentExtractorList() {
        List<ArgumentExtractor> l = new ArrayList<>();
        l.add(casArgumentExtractor());
        return l;
    }
}
