package com.digitalmisfits.cas.web.config.cas.client;

import org.jasig.cas.client.validation.Cas20ServiceTicketValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.cas.ServiceProperties;
import org.springframework.security.cas.authentication.CasAuthenticationProvider;
import org.springframework.security.cas.userdetails.GrantedAuthorityFromAssertionAttributesUserDetailsService;
import org.springframework.security.cas.web.CasAuthenticationEntryPoint;
import org.springframework.security.cas.web.CasAuthenticationFilter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;

@Configuration
@ComponentScan
public class CentralAuthorityServiceClientConfiguration {

    @Bean
    @Autowired
    public CasAuthenticationFilter casFilter(
            @Qualifier("myAuthenticationManager") AuthenticationManager authenticationManager ) throws Exception {
        CasAuthenticationFilter filter = new CasAuthenticationFilter();
        filter.setAuthenticationManager(authenticationManager);
        return filter;
    }

    @Bean
    public ServiceProperties serviceProperties() {
        ServiceProperties serviceProperties = new ServiceProperties();
        serviceProperties.setService("https://localhost:8443/j_spring_cas_security_check");
        serviceProperties.setSendRenew(false);
        return serviceProperties;
    }

    @Bean
    public CasAuthenticationProvider casAuthenticationProvider() {
        CasAuthenticationProvider authenticationProvider = new CasAuthenticationProvider();
        authenticationProvider.setAuthenticationUserDetailsService(attributesUserDetailsService());
        authenticationProvider.setServiceProperties(serviceProperties());
        authenticationProvider.setTicketValidator(ticketValidator());
        authenticationProvider.setKey("defaultCasAuthenticationProvider");
        return authenticationProvider;
    }

    @Bean
    public GrantedAuthorityFromAssertionAttributesUserDetailsService attributesUserDetailsService() {
        return new GrantedAuthorityFromAssertionAttributesUserDetailsService(new String[] {"authorities"});
    }

    @Bean
    public CasAuthenticationEntryPoint casEntryPoint() {
        CasAuthenticationEntryPoint entryPoint = new CasAuthenticationEntryPoint();
        entryPoint.setLoginUrl("https://localhost:8443/cas/login");
        entryPoint.setServiceProperties(serviceProperties());
        return entryPoint;
    }

    @Bean
    public Cas20ServiceTicketValidator ticketValidator() {
       return new Cas20ServiceTicketValidator("https://localhost:8443/cas/p3/");
    }
}
