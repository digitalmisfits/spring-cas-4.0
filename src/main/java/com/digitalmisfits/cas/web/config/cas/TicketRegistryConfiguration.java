package com.digitalmisfits.cas.web.config.cas;

import com.digitalmisfits.support.compat.jasig.cas.ticket.registry.support.DefaultTicketCleanerTask;
import org.jasig.cas.logout.LogoutManager;
import org.jasig.cas.ticket.registry.DefaultTicketRegistry;
import org.jasig.cas.ticket.registry.RegistryCleaner;
import org.jasig.cas.ticket.registry.TicketRegistry;
import org.jasig.cas.ticket.registry.support.DefaultTicketRegistryCleaner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

@Configuration
@EnableScheduling
public class TicketRegistryConfiguration {

    @Autowired
    private CentralAuthorityServiceConfiguration centralAuthorityServiceConfiguration;

    @Bean
    public TicketRegistry ticketRegistry() {
        return new DefaultTicketRegistry();
    }

    @Bean
    @Autowired
    public RegistryCleaner ticketRegistryCleaner(LogoutManager logoutManager) {
        DefaultTicketRegistryCleaner ticketRegistryCleaner = new DefaultTicketRegistryCleaner();
        ticketRegistryCleaner.setTicketRegistry(ticketRegistry());
        ticketRegistryCleaner.setLogoutManager(logoutManager);
        return ticketRegistryCleaner;
    }

    @Bean
    public DefaultTicketCleanerTask ticketCleanerTask() {
        return new DefaultTicketCleanerTask();
    }
}
