package com.digitalmisfits.cas.config;

import com.digitalmisfits.cas.domain.entity.Marker;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.hibernate.dialect.MySQLDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.ClassUtils;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.util.Properties;

/**
 * The envTarget is specified in the maven pom.xml; can be overridden using environmental variables or java arguments
 */
@Configuration
@PropertySources(value = {@PropertySource("classpath:persistence-${envTarget:dev}.properties")})
@EnableTransactionManagement
@EnableJpaRepositories(basePackageClasses = com.digitalmisfits.cas.domain.repository.Marker.class)
@ComponentScan(basePackageClasses = {com.digitalmisfits.cas.domain.repository.service.Marker.class})
public class PersistenceConfiguration {

    /**
     * The annotation @Value can no longer be used in custom beans, use @Environment instead
     */
    @Autowired
    private Environment environment;

    @Bean(destroyMethod = "close")
    public DataSource datasource() {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();

        try {
            dataSource.setDriverClass(ClassUtils.getQualifiedName(com.mysql.jdbc.Driver.class));
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }

        dataSource.setJdbcUrl(environment.getRequiredProperty("database.url"));
        dataSource.setUser(environment.getRequiredProperty("database.username"));
        dataSource.setPassword(environment.getRequiredProperty("database.password"));

        // c3p0 properties
        dataSource.setAcquireIncrement(environment.getRequiredProperty("c3p0.acquireIncrement", Integer.class));
        dataSource.setMinPoolSize(environment.getRequiredProperty("c3p0.minPoolSize", Integer.class));
        dataSource.setMaxPoolSize(environment.getRequiredProperty("c3p0.maxPoolSize", Integer.class));
        dataSource.setMaxIdleTime(environment.getRequiredProperty("c3p0.maxIdleTime", Integer.class));

        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(datasource());

        factoryBean.setPackagesToScan(new String[]{ClassUtils.getPackageName(Marker.class)});

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter() {
            {
                setDatabasePlatform(ClassUtils.getQualifiedName(MySQLDialect.class));
                setShowSql(false);
                setGenerateDdl(true);
            }
        };

        factoryBean.setJpaVendorAdapter(vendorAdapter);
        factoryBean.setJpaProperties(jpaProperties());
        return factoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        transactionManager.setJpaDialect(new HibernateJpaDialect());
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean
    public LocalValidatorFactoryBean validatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }

    final Properties jpaProperties() {
        return new Properties() {
            private static final long serialVersionUID = 1L;

            {
                this.put("hibernate.hbm2ddl.auto", "update");
            }
        };
    }
}