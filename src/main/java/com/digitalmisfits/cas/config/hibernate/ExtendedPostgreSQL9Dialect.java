package com.digitalmisfits.cas.config.hibernate;

import org.hibernate.dialect.PostgreSQL9Dialect;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.StandardBasicTypes;

/**
 * Registers array_agg as function in JPQL
 */
public class ExtendedPostgreSQL9Dialect extends PostgreSQL9Dialect {

    public ExtendedPostgreSQL9Dialect() {
        super();

        registerFunction("array_agg", new StandardSQLFunction("array_agg", StandardBasicTypes.STRING));
    }
}
