package com.digitalmisfits.cas.config;

import com.digitalmisfits.cas.web.config.WebSecurityConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackageClasses = {
        com.digitalmisfits.cas.security.Marker.class
})
@Import({WebSecurityConfiguration.class})
public class SecurityConfiguration {

}
